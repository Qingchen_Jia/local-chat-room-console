package LocalChatRoom;

public class User {
    private String userId;
    private String passwd;

    public User(String userId, String passwd) {
        this.userId = userId;
        this.passwd = passwd;
    }

    public String getUserId() {
        return userId;
    }

    public String getPasswd() {
        return passwd;
    }

    @Override
    public String toString() {
        return userId + "=" + passwd;
    }
}
