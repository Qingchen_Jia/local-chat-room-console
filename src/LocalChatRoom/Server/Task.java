package LocalChatRoom.Server;

import LocalChatRoom.Server.Server;
import LocalChatRoom.User;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;

public class Task implements Runnable {
    public static final ArrayList<Socket> clientSockets = new ArrayList<>();
    private Socket socket;
    private BufferedReader serverRead;
    private BufferedWriter serverWrite;
    private Properties userInfo = new Properties();
    private User user;

    public Task(Socket socket) throws IOException {
        this.socket = socket;
        clientSockets.add(socket);
        serverRead = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        serverWrite = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        userInfo.load(new FileReader("userInfo"));
    }

    @Override
    public void run() {
        try {
            while (true) {
                String choice = serverRead.readLine();  //获取客户端功能状态
                if (String.valueOf(Server.LOGIN).equals(choice)) {
                    if ("SUCCESS!".equals(login()))
                        chat();
                } else if (String.valueOf(Server.REGISTER).equals(choice)) register();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void feedback(String message) throws IOException {  //向客户端发送反馈信息
        serverWrite.write(message);
        serverWrite.newLine();
        serverWrite.flush();
    }

    private void feedback(String message, BufferedWriter bufferedWriter) throws IOException {   //向指定客户端发送消息
        bufferedWriter.write(message);
        bufferedWriter.newLine();
        bufferedWriter.flush();
    }

    private String login() throws IOException {
        String data = serverRead.readLine();
        String userId = data.split("=")[0];
        String passwd = data.split("=")[1];
        String message;
        user = new User(userId, passwd);

        //校验用户名和密码
        if (!userInfo.containsKey(userId)) {
            message = "NOT EXIST CURRENT USER";
            feedback(message);
        } else {
            if (userInfo.get(userId).equals(passwd)) {
                message = "SUCCESS!";
                feedback(message);
            } else {
                message = "ERROR PASSWORD";
                feedback(message);
            }
        }

        return message;
    }

    private void register() throws IOException {
        String data = serverRead.readLine();
        String userId = data.split("=")[0];
        String passwd = data.split("=")[1];

        if (userInfo.containsKey(userId)) {
            feedback("ALREADY EXIST CURRENT USER");
        } else {
            userInfo.setProperty(userId, passwd);
            userInfo.store(new FileWriter("userInfo"), null);
            feedback("SUCCESS!");
        }
    }

    private void chat() throws IOException {
        while (true) {
            String sentence = serverRead.readLine();
            if (!sentence.isEmpty()) {
                if (String.valueOf(Server.END_CHAT).equals(sentence)) break;
                else {
                    System.out.println(user.getUserId() + ":" + sentence);
                    forward(user.getUserId() + ":" + sentence);
                }
            }
        }

        feedback("End chat");
    }

    private void forward(String sentence) throws IOException {
        for (Socket clientSocket : clientSockets) {
            BufferedWriter distribute = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            feedback(sentence, distribute);
        }
    }
}
