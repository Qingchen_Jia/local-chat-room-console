package LocalChatRoom.Server;

import LocalChatRoom.Client.Client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    public static final int LOGIN = 1; //登录状态
    public static final int REGISTER = 2;  //注册状态
    public static final int END_CHAT = 3;  //聊天结束

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(Client.PORT);
        ExecutorService executorService = Executors.newCachedThreadPool();  //创建线程池

        while (true) {
            Socket clientSocket = serverSocket.accept();
            executorService.submit(new Task(clientSocket));   //验证用户登录
        }
    }
}
