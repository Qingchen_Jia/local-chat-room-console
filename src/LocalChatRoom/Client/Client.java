package LocalChatRoom.Client;

import LocalChatRoom.Server.Server;
import LocalChatRoom.User;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {
    public static final Scanner INPUT = new Scanner(System.in);
    public static final int PORT = 23456;  //端口
    private static BufferedWriter socketWrite;
    private static BufferedReader socketRead;
    private static ExecutorService executorService;    //线程池

    public static void main(String[] args) throws IOException {
        //创建套接字连接本地和开启输入输出流
        Socket socket = new Socket("127.0.0.1", PORT);  //连接本地
        socketWrite = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        socketRead = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        executorService = Executors.newCachedThreadPool();  //创建线程池

        while (true) {
            menu();
            String choice;
            choice = INPUT.next();
            switch (choice) {
                case "1" -> {
                    if (login()) chat(socket);
                }
                case "2" -> register();
                default -> System.out.println("无效选项，请重新输入");
            }
        }
    }

    private static boolean login() throws IOException {  //登录
        //向服务器声明登录操作
        transmit(String.valueOf(Server.LOGIN));

        //输入用户名和密码
        System.out.println("userId>");
        String userId = INPUT.next();
        System.out.println("passwd>");
        String passwd = INPUT.next();
        User user = new User(userId, passwd);

        //将用户名和密码传输到服务端校验
        transmit(user.toString());

        //校验反馈
        String feedback = socketRead.readLine();
        System.out.println(feedback);

        if ("SUCCESS!".equals(feedback)) return true;
        else return false;
    }

    private static void register() throws IOException { //注册
        //向服务器声明注册操作
        transmit(String.valueOf(Server.REGISTER));

        //输入用户名和密码
        System.out.println("userId>");
        String userId = INPUT.next();
        System.out.println("passwd>");
        String passwd = INPUT.next();
        User user = new User(userId, passwd);

        //将用户名和密码传输到服务端校验
        transmit(user.toString());

        //校验反馈
        String feedback = socketRead.readLine();
        System.out.println(feedback);
    }

    private static void chat(Socket socket) throws IOException {
        executorService.submit(new Receive(socket));

        while (true) {
            String sentence = INPUT.nextLine();
            transmit(sentence);

            if ("bye".equals(sentence)) {   //结束聊天
                transmit(String.valueOf(Server.END_CHAT));
                break;
            }
        }

        String feedback = socketRead.readLine();
        System.out.println(feedback);
    }

    private static void transmit(String message) throws IOException {   //向服务端发送信息
        socketWrite.write(message);
        socketWrite.newLine();
        socketWrite.flush();
    }

    private static void menu() {
        System.out.println("***本地聊天室***");
        System.out.println("1.登录");
        System.out.println("2.注册");
    }
}
