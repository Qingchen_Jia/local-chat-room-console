package LocalChatRoom.Client;

import java.io.*;
import java.net.Socket;

public class Receive implements Runnable {
    private Socket socket;

    public Receive(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            while (true) {
                BufferedReader getSentence = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String sentence = getSentence.readLine();
                System.out.println(sentence);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
